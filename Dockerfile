FROM alpine:3.17.3
ARG restic_version=0.12.0

RUN apk update && apk upgrade

RUN apk add --no-cache wget bzip2 bash

WORKDIR /tmp

# Restic 0.12.0
RUN wget https://github.com/restic/restic/releases/download/v${restic_version}/restic_${restic_version}_linux_amd64.bz2

RUN bzip2 -d restic_${restic_version}_linux_amd64.bz2

RUN mv  restic_${restic_version}_linux_amd64 restic

RUN cp /tmp/restic /bin/
RUN mv /tmp/restic /usr/bin/

RUN mkdir /config

RUN mkdir scripts

COPY scripts scripts

WORKDIR scripts

CMD ["./forge_restic.sh"]
