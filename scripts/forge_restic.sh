#!/bin/bash

set -e -o pipefail -o errexit -o errtrace

display_time_used()
{
    local TZ=UTC
    local msg=$1
    local sec=$2
    printf '%s: %02dh:%02dm:%02dsT\n' $((secs/3600)) $((secs%3600/60)) $((secs%60))
}


integrity_check()
{
    # $1: Setting to determine if restic will only check the repo or the repo and the data
    # Available values:
    # - a number between 0 and 100 (with a '%' at the end) that will be the pourcentage of data to check
    # - a fraction that represent the portion to check (2/5 will check the second data portion)
    # - 'no-data-read' or 'simple' to perform a simple check on the repo (same as 0%)
    # - 'all-data' to perform full check on the repo (same as 100%)
    # - an empty vars means no check

    local integrity_check_setting="$DATA_INTEGRITY_CHECK"

    if [ -z "${integrity_check_setting}" ]; then
        echo "No restic checks to run."
    else
        echo "Running restic check on the repo to ensure that everything is alright."
        case "${integrity_check_setting}" in
            100%|all-data)
                echoInfo "Checking all the data in the restic repository."
                restic check --read-data
                ;;
            0%|no-data-read|simple)
                restic check
                ;;
            *)
                echo "Checking the data with the setting: ${integrity_check_setting}.."
                restic check --read-data-subset="${integrity_check_setting}"
                ;;
        esac
    fi
}

prune()
{
    local start_time="${SECONDS}"

    echo "Starting prune"
    echo "Prune policy: ${BACKUP_FORGET_POLICY}"

    restic forget --host "$HOSTNAME" ${BACKUP_FORGET_POLICY} --prune "$@"

    display_time_used "Pruning finished in" $(( SECONDS - start_time ))
}

main()
{

    local start_time=${SECONDS}

    ## Endpoint conf
    export S3_CONNECTION_SCHEME="${S3_CONNECTION_SCHEME:-"https"}"
    export S3_HOST="${S3_HOST:-"restic-backup.undercloud.cri.epita.fr"}"
    export S3_PORT="${S3_PORT:-443}"

    ## Auth conf
    export AWS_ACCESS_KEY_ID="${AWS_ACCESS_KEY_ID}"
    export AWS_SECRET_ACCESS_KEY="${AWS_SECRET_ACCESS_KEY}"

    # Bucket conf
    export S3_BUCKET_NAME="${S3_BUCKET_NAME}"
    export RESTIC_REPOSITORY="${RESTIC_REPOSITORY:-s3:${S3_CONNECTION_SCHEME}://${S3_HOST}:${S3_PORT}/${S3_BUCKET_NAME}}"

    
    ## Restic password, used to encrypt repository content
    export RESTIC_PASSWORD="${RESTIC_PASSWORD}"

    ## Restic forget policy, used to tell restic which snapshot it should keep
    export BACKUP_FORGET_POLICY="${BACKUP_FORGET_POLICY:-"--keep-daily 7 --keep-weekly 8 --keep-monthly 12 --keep-yearly 2"}"

    ## Restic integrity checks settings
    export DATA_INTEGRITY_CHECK="${DATA_INTEGRITY_CHECK:-100%}"

    ## Integrity
    export PRE_INTEGRITY_CHECK="${PRE_INTEGRITY_CHECK:-false}"
    export POST_INTEGRITY_CHECK="${POST_INTEGRITY_CHECK:-false}"
    
    # Run function
    [ "$PRE_INTEGRITY_CHECK" = "true" ] && integrity_check
    prune
    [ "$POST_INTEGRITY_CHECK" = "true" ] && integrity_check

    display_time_used "Total Duration for $HOSTNAME" $(( SECONDS - start_time ))
}

[[ "$0" == "${BASH_SOURCE[0]}" ]] && main
